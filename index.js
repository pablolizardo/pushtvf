const axios = require('axios')
var prompt = require('prompt');
var colors = require("colors/safe");
var clear = require('clear');

var endpoint = 'https://exp.host/--/api/v2/push/send'
var tvfuegoToken = 'CIVQCLPjQkwvvBpK9dWYhZPgozEMcuR3B2nFpJ4fdo'
var subscribers_api = 'https://www.tvfuego.com.ar/api/v4/userToken'
var subscribers_api_test = 'http://tvfuego.test/api/v4/userToken'
let subscribers = []
let test = false
let message = {
  title: 'TVFUEGO',
  body: 'Siempre en casa, siempre con vos',
  screeen: 'internet',
  content: ''
}

clear();
menu()
function menu(){

  prompt.start();
  prompt.message = colors.yellow("TVFUEGO Push");

  console.log('\n\n\tOpciones\n')
  console.log('\t1 : Ver suscriptores')
  console.log('\t2 : Enviar un mensaje a todos')
  console.log('\t3 : Enviar un mensaje individual')
  console.log('\t4 : Salir')
  console.log('\t\n\n')

  prompt.get([{name: 'opcion', description:'Elija su opcion' , type:'integer', required: true}], function (err, result) {
    // console.log('A todos? ' + result.todos);
    if(result.opcion == 1) getSubscribers()
    if(result.opcion == 2) compose().then(()=>sendPushToAll())
    if(result.opcion == 3){
      compose().then(()=>{
        prompt.get([{name: 'token', description:'Ingrese token' , type:'string', required: true}], function (err, result) {
          console.log('token Ingresado: ' + result.token);
          sendPush(result.token)
        });
      })
    }
    if(result.opcion == 4) return process.exit()   

  });
}

function compose(){
  return new Promise((resolve,reject) => {
    prompt.message = colors.yellow("Nueva Notificacion");
    prompt.get([
      {name: 'title', description:'Titulo' , type:'string', required: true},
      {name: 'body', description:'Subtitulo' , type:'string', required: true},
      {name: 'screen', description:'Redirigir a (internet, telefonia, television, promociones, notificaciones)' , type:'string', required: true},
      {name: 'content', description:'Contenido' , type:'string', required: true},

    ], function (err, result) {
      message.title = result.title
      message.body = result.body
      message.screen = result.screen
      message.content = result.content
      resolve(message)
    });
  })
}
function getSubscribers() {
  const headers = { Authorization: tvfuegoToken } 
  console.log('Obteniendo Suscriptores')
  axios
    .get(test?subscribers_api_test:subscribers_api,{ headers: headers })
      .then(response => {
        subscribers = response.data
        printSubs()
        menu()
      })
      .catch(e => console.log(e)
      )
}
function printSubs(){
  clear();
  console.log(`\n\t${subscribers.length} suscriptor(es) obtenidos\n`)
  console.log(`\tID\tCLIENTE\tTOKEN\t\t\t\t\t\tNOMBRE`)

  subscribers.sort((a,b)=>{ return a.id-b.id }).map(s=> 
    console.log(`\t#${s.id}\t${s.nrocliente}\t${s.token}\t${s.username} `) )
}
function sendPushToAll() {
  if(subscribers.length == 0){ getSubscribers() }
  subscribers.map(client => { sendPush(client.token) })
}
function sendPush(token) {
  const headers = {
    'host': 'exp.host',
    'accept': 'application/json',
    'accept-encoding': 'gzip, deflate',
    'content-type': 'application/json'
  }
  const data = {
    to: token,
    sound: 'default',
    title: message.title,
    body: message.body,
    data: {
      screen: message.screen,
      content: message.content
    },
    _displayInForeground: true,
  }
  axios
    .post(endpoint, data, { headers: headers })

}
